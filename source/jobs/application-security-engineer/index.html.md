---
layout: job_page
title: "Senior Application Security Engineer"
---

As a key member of our application security team, your core responsibility is to work with functional groups across GitLab to assess the security architecture of new products and capabilities. Examples include executing and maintaining a security review program, and working with development teams to define and evangelize security best practices.

Our thesis is that [Good Security Is Holistic](https://medium.com/@justin.schuh/stop-buying-bad-security-prescriptions-f18e4f61ba9e). We think that simulating a security culture in engineering is one of the most important things. We don't do checklist security, the goal is to keep the trust of our users by being secure, compliance is not a goal in itself. We don't think that third party products are unimportant but they are not a silver bullet to making everything secure.

The [Security Team](/handbook/engineering/security) is responsible for leading and implementing the various initiatives that relate to improving GitLab's security.

## Responsibilities

- Own vulnerability management and mitigation approaches
- Conduct threat modeling tied to security services
- Conduct application security reviews
- Implement secure architecture design
- Provide security training and outreach to internal development teams
- Develop security guidance documentation
- Assist with recruiting activities and administrative work
- Define, implement, and monitor security measures to protect GitLab.com and company assets


## Requirements

- Familiarity with common security libraries, security controls, and common security flaws that apply to Ruby on Rails applications
- Some development experience (Ruby and Ruby on Rails preferred; for GitLab debugging)
- Experience with OWASP, static/dynamic analysis, and common exploit tools and methods
- An understanding of network and web related protocols (such as, TCP/IP, UDP, IPSEC, HTTP, HTTPS, routing protocols)
- Excellent written and verbal communication skills
- Demonstrable teamwork skills and resourcefulness
- Familiarity with cloud security controls and best practices
- Passion for open source
- Linux experience (e.g. Ubuntu)
- Network security experience (Routing, firewalls, VPNs, common services and protocols)
- Collaborative team spirit with great communication skills
- You share our [values](handbook/values/), and work in accordance with those values.

## Hiring Process

Applicants for this position can expect the hiring process to follow the order below. Please keep in mind that applicants can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our [team page](https://about.gitlab.com/team).

- Selected candidates will be invited to schedule a screening call with a Recruiter
- Next, candidates will be invited to schedule a 45 minute technical interview with the Security Lead
- Candidates will then be invited to schedule a 45 minute interview with our Director of Security
- Candidates will be invited to schedule a one hour interview with our VP of Engineering
- Finally, candidates may have a 50 minute interview with our CEO
- Successful candidates will subsequently be made an offer via email

Additional details about our process can be found on our [hiring page](/handbook/hiring).

## Apply

Please note that if we are actively hiring for a position, you will see it listed on our [jobs page](https://about.gitlab.com/jobs/), where all of our current openings are advertised. To apply, please click on the name of the role you are interested in, which will take you to our applicant tracking system (ATS), [Lever](https://www.lever.co/).

Avoid the [confidence gap](https://www.theatlantic.com/magazine/archive/2014/05/the-confidence-gap/359815/
); you do not have to match all the listed requirements exactly to apply. Our hiring process is described in more detail in our [hiring handbook](https://about.gitlab.com/handbook/hiring/).

## About GitLab

GitLab Inc. is a company based on the GitLab open-source project. GitLab is a community project to which over 1,000 people worldwide have contributed. We are an active participant in this community, trying to serve its needs and lead by example. We have one [vision](https://about.gitlab.com/strategy/): everyone can contribute to all digital content, and our mission is to change all creative work from read-only to read-write so that everyone can contribute.

We [value](https://about.gitlab.com/handbook/values/) results, transparency, sharing, freedom, efficiency, frugality, collaboration, directness, kindness, diversity, boring solutions, and quirkiness. If these values match your personality, work ethic, and personal goals, we encourage you to visit our [primer](https://about.gitlab.com/primer/) to learn more. Open source is our culture, our way of life, our story, and what makes us truly unique.

Top 10 reasons to work for GitLab:

1. Work with helpful, kind, motivated, and talented people.
1. Work remote so you have no commute and are free to travel and move.
1. Have flexible work hours so you are there for other people and free to plan the day how you like.
1. Everyone works remote, but you don't feel remote. We don't have a head office, so you're not in a satellite office.
1. Work on open source software so you can interact with a large community and can show your work.
1. Work on a product you use every day: we drink our own wine.
1. Work on a product used by lots of people that care about what you do.
1. As a company we contribute more than we take, most of our work is released as the open source GitLab CE.
1. Focused on results, not on long hours, so that you can have a life and don't burn out.
1. Open internal processes: know what you're getting in to and be assured we're thoughtful and effective.

See [our culture page](https://about.gitlab.com/culture) for more!

Work remotely from anywhere in the world. Curious to see what that looks like? Check out our [remote manifesto](https://about.gitlab.com/2015/04/08/the-remote-manifesto/).

